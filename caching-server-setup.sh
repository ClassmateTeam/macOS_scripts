# Enable Network Time
/usr/sbin/systemsetup -setusingnetworktime on
# Disable Sleep
/usr/sbin/systemsetup -setsleep off
/usr/sbin/systemsetup -setcomputersleep off
/usr/sbin/systemsetup -setallowpowerbuttontosleepcomputer off
# Enable Wake on Network Access
/usr/sbin/systemsetup -setwakeonnetworkaccess on
# Enable Restart after Power Failure
/usr/sbin/systemsetup -setrestartpowerfailure on
# Enable Restart on Freeze
/usr/sbin/systemsetup -setrestartfreeze on
# Disable WiFi
/usr/sbin/networksetup -setnetworkserviceenabled "Wi-Fi" off
# Set Volume to 0
/usr/sbin/nvram SystemAudioVolume=%00

sed -i -e 's/.PermitRootLogin.*/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i -e 's/.MaxStartups.*/MaxStartups 3/g' /etc/ssh/sshd_config
sed -i -e 's/.LoginGraceTime.*/LoginGraceTime 30/g' /etc/ssh/sshd_config
sed -i -e 's/.GSSAPIAuthentication.*/GSSAPIAuthentication no/g' /etc/ssh/sshd_config
sed -i -e 's/.ClientAliveInterval.*/ClientAliveInterval 15m/g' /etc/ssh/sshd_config
sed -i -e 's/.ClientAliveCountMax.*/ClientAliveCountMax 0/g' /etc/ssh/sshd_config