#!/bin/bash
####################################################
## Set $4 to SSID
## Set $5 to SSID Password
## Set $6 to Username (computer create and read (JSS Objects), Send Computer Unmanage Command (JSS Actions))
## Set $7 to Password
## Set $8 to exisiting MDM ServerURL (Optional)
####################################################
ssid="$4"
ssidPassword="$5"
username="$6"
password="$7"
if [ -n "$8" ];then
	serverURL="$8"
else
	## Get current Jamf serverURL
	serverURL=$(defaults read /Library/Preferences/com.jamfsoftware.jamf.plist jss_url)
fi
# Find Wi-Fi interface
wifiInterface=$(/usr/sbin/networksetup -listallhardwareports | awk '/^Hardware Port: (Wi-Fi|AirPort)/,/^Device/' | tail -1 | cut -c 9-)
# Find current Wi-Fi network
currentWifi=$(/usr/sbin/networksetup -getairportnetwork "$wifiInterface" | cut -c 24-)


# Function to remove MDM
removeMDM() {
  ## Ensure the serverURL ends with /
  strLen=$((${#serverURL}-1))
  lastChar="${serverURL:$strLen:1}"
  if [ ! "$lastChar" = "/" ];then
      serverURL="${serverURL}/"
  fi

  ## Get UUID
  udid=$(system_profiler SPHardwareDataType | awk '/UUID/ { print $3; }')

  ## Get Mac ID from Jamf Pro
  macID=$(/usr/bin/curl -sku ${username}:${password} ${serverURL}JSSResource/computers/udid/${udid}/subset/general -H "Accept: application/xml" | /usr/bin/xpath "//computer/general/id/text()" )

  ## Send unmanage MDM command
  curl -X POST -sku ${username}:${password} ${serverURL}JSSResource/computercommands/command/UnmanageDevice/id/${macID}
}

# Remove framework
removeFramework() {
  $jamfCLIPath removeFramework
}

# Join Wi-Fi network
joinWiFi() {
  /usr/sbin/networksetup -setairportnetwork "$wifiInterface" "$ssid" "$ssidPassword"
}

# Ensure jamf binary is ready
jamfCLIPath=/usr/local/jamf/bin/jamf
/usr/sbin/chown 0:0 $jamfCLIPath
/bin/chmod 551 $jamfCLIPath

if [ "$currentWifi" == "$ssid" ]; then
  removeMDM
  /bin/sleep 30
  joinWiFi
  removeFramework
else
  removeMDM
  removeFramework
fi

# Exit
exit 0
