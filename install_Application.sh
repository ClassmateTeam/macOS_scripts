#!/bin/bash
#################### Variables ####################
# Set $4 to "Download URL".  Optionally set it here:
## Example: DownloadURL="https://dl.google.com/chrome/mac/stable/googlechrome.dmg" or DownloadURL="https://dl.google.com/drive-file-stream/GoogleDriveFileStream.dmg"
DownloadURL=""
################## Do Not Modify ##################

# Incase we didn't specify something
## Download URL
if [[ $4 ]]; then
  DownloadURL="$4"
elif [[ -z $DownloadURL ]]; then
  echo "No DownloadURL specified"
  exit 1
fi

# Create directory /tmp/jamf, continue if directory already exists
mkdir /tmp/jamf || :

# Change directory to /tmp/jamf
cd /tmp/jamf

#Download installer container into /tmp/jamf
# -O downloads file without changing its name
curl $DownloadURL -O -L

# If container is a .dmg:
# Mount installer container
# -nobrowse to hide the mounted .dmg
# -noverify to skip .dmg verification
# -mountpoint to specify mount point
yes | hdiutil attach /tmp/jamf/*.dmg -nobrowse -noverify -mountpoint /tmp/jamf/mount ||
# Else if container is a .pkg
# Run installer package with the boot drive as the destination
installer -pkg /tmp/jamf/*.pkg -target / ||
# Else if container is anything else, presumably a zip file:
# Unzip installer container and place contents into /tmp/jamf/mount, continue on error
unzip /tmp/jamf/* -d /tmp/jamf/mount || :

# If contents is installer .pkg:
# Run installer package with the boot drive as the destination
installer -pkg /tmp/jamf/mount/*.pkg -target / ||
# Else if contents is .app:
# Copy the .app file from the installer container to /Applications
# Preserve all file attributes and ACLs
cp -pPR /tmp/jamf/mount/*.app /Applications || :

# Unmount the secondary installation folder, continue on error
hdiutil detach /tmp/jamf/mount || :

# Delete the main installation folder
rm -r /tmp/jamf
