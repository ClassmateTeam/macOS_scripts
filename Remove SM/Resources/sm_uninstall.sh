#!/usr/bin/env bash

launchctl unload /Library/LaunchDaemons/com.meraki.agentd.plist
rm -f /usr/sbin/m_agent /usr/sbin/m_agent_upgrade
rm -rf '/Library/Application Support/Meraki/'
rm -f /Library/LaunchDaemons/com.meraki.agentd.plist
/bin/echo "y" | /usr/bin/profiles -D
if [[ $? != 0 ]]; then
  echo "Error!"
  exit 1
else
  echo "All done!"
  exit 0
fi
