#!/bin/bash
####################################################
## Set $4 to Username (computer create and read (JSS Objects), Send Computer Unmanage Command (JSS Actions))
## Set $5 to Password
## Set $6 to exisiting MDM ServerURL (Optional)
####################################################
username="$4"
password="$5"

if [ -n "$6" ];then
	serverURL="$6"
else
	## get current Jamf serverURL
	serverURL=$(defaults read /Library/Preferences/com.jamfsoftware.jamf.plist jss_url)
fi

## ensure the serverURL URL ends with a /
strLen=$((${#serverURL}-1))
lastChar="${serverURL:$strLen:1}"
if [ ! "$lastChar" = "/" ];then
    serverURL="${serverURL}/"
fi

## get unique identifier for machine
udid=$(system_profiler SPHardwareDataType | awk '/UUID/ { print $3; }')

## get computer ID from Jamf serverURL
compId=$(/usr/bin/curl -sku ${username}:${password} ${serverURL}JSSResource/computers/udid/${udid}/subset/general -H "Accept: application/xml" | /usr/bin/xpath "//computer/general/id/text()" )

## send unmanage command to machine
curl -X POST -sku ${username}:${password} ${serverURL}JSSResource/computercommands/command/UnmanageDevice/id/${compId}
