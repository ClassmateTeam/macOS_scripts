# Migrating macOS from one Jamf Pro to another
## Instructions
1. Create a **policy with 3 items** on the **old Jamf Pro Server**
  1. aRemove_MDM.sh script
  2. zConnect_WiFi.sh script
  3. QuickAdd.pkg package for the new Jamf Pro Server
2. Be sure both scripts are set to run **before other actions**

## Explanation
* aRemove_MDM.sh will utilize the existing Jamf Pro's API to remove the MDM profile from macOS.
* Since removing MDM typically causes macOS to loose WiFi connectivity, zConnect_WiFi.sh will re-establish the WiFi connection.
* QuickAdd.pkg will upgrade the Jamf Binary and complete the enrollment into the new Jamf Pro Server.
