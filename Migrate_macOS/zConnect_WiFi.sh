#!/bin/sh
####################################################
## Set $4 to SSID
## Set $5 to SSID Password
####################################################
# Sleep for a minute to ensure MDM profile has a chance to remove WiFi
/bin/sleep 60

# Find Wi-Fi interface
wifiInterface=$(/usr/sbin/networksetup -listallhardwareports | awk '/^Hardware Port: (Wi-Fi|AirPort)/,/^Device/' | tail -1 | cut -c 9-)

# Join Wi-Fi network
/usr/sbin/networksetup -setairportnetwork "$wifiInterface" "$4" "$5"

# Sleep for 30 seconds to ensure WiFi completes handshake
/bin/sleep 30
