#!/bin/bash
# Jamf Pro variables
## $4 = Queue Name (no spaces)

/usr/sbin/lpadmin -x $4
if [ "$?" == 0 ]; then
    echo "Printer $4 Removed"
    exit 0
else
    echo "Error occured: $?"
    exit 1
fi
