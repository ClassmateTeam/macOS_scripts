#!/bin/bash
list=$(/usr/bin/lpstat -a | awk '{print $1}')
for i in $list; do
	/usr/sbin/lpadmin -x $i
	/bin/echo "Removed $i"
done
