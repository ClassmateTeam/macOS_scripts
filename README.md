# Random macOS Scripts
Deployment and Management of macOS, primarily using Jamf Pro

## DEPNotify
A collection of scripts used to implement DEPNotify into your existing deployment workflow.

## AutoPkg
A number of scripts used to deploy, configure, and report on the AutoPkg binary.

## Git
A couple scripts used to deploy and report on the Git binary.

## macOS Auto Updates
coming soon...

## vmip
`vmip` can be placed within your $PATH and executed to display running VM's and their IP Addresses.  Tested with VMware Fusion [Pro] 7+.

## enroll_macOS.sh
Designed to work within Jamf Pro, this will allow easy migration of macOS devices from one Jamf Pro server to another. Be sure to set variables **$4 to the new Jamf Pro URL** & **$5 to an Enroll Invitation Code**.

## Unenroll_macOS.sh
Removes a Mac from it's current Jamf Pro server.

## Printers
Two scripts capable of installing printers and enabling options/accessories.
