#!/bin/bash
################ Variables ################
# Set $4 to "Install macOS xxx.app"
os_app=""
############## Do Not Modify ##############
if [[ $4 ]]; then
  os_app=$4
fi
if [[ -z $os_app ]]; then
  logger -s "$os_app not defined.  Aborting..."
  exit 1
elif [[ -d "/Applications/$os_app" ]]; then
    logger -s "Starting $os_app Upgrade..."

     "/Applications/$os_app/Contents/Resources/startosinstall" --applicationpath "/Applications/$os_app" --agreetolicense --rebootdelay 10 --nointeraction
     sleep 60
else
    logger -s "$os_app not found.  Aborting..."
    exit 1
fi
exit 0
